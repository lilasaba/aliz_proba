"""Module for ThresholdBinarizer and custom_estimator classes."""

import numpy as np
import pandas as pd

from sklearn.base import BaseEstimator, ClassifierMixin, TransformerMixin
from sklearn.linear_model import LogisticRegression


class ThresholdBinarizer(BaseEstimator, TransformerMixin):
    """
    Class to binarize decision scores of a logistic regression classifier.

    Parameters
    ----------

    Attributes
    ----------
    best_gini_ : float, best gini impurity index after running self.binarize.
    threshold_ : float, best threshold after running self.binarize.

    Examples
    --------
    """
    def __init__(self):
        pass

    def split_by_value_(self, value, X, y):
        """
        Split list of scores by a value.

        Parameters
        ----------
        value : float, value to split X at.

        X : array_like or list, decision scores from the classifier.

        y : list, class values.

        Returns
        -------
        left : list, list of tuples (X[i], y[i]).

        right : list, list of tuples (X[i], y[i]).
        """
        assert len(X) == len(y)
        left, right = [], []

        for i in range(len(X)):
            if X[i] < value:
                left.append((X[i], y[i]))
            else:
                right.append((X[i], y[i]))
        return left, right

    def get_gini_index_(self, groups, classes):
        """
        Calculate Gini impurity for a split dataset.

        Parameters
        ----------
        groups : tuple, tuple of list of tuples (decision_score, class_value).

        classes : array-like or list, unique class values.

        Returns
        -------
        gini : float, Gini index for the split.
        """
        # Nr. of all samples at split point.
        n_instances = float(sum([len(group) for group in groups]))
        # Sum weighted Gini index for each group.
        gini = 0.0
        for group in groups:
            size = float(len(group))
            # Avoid division by zero.
            if size == 0:
                continue
            score = 0.0
            # Get group score based on the score for each class.
            for class_val in classes:
                p = [tup[-1] for tup in group].count(class_val) / size
                score += p * p
            # Weight the group score by its relative size.
            gini += (1.0 - score) * (size / n_instances)
        return gini

    def get_split_(self, X, y):
        """
        Select the best split point for a dataset.

        Parameters
        ----------
        X : array-like or list, decision scores.

        y : array-like or list, list of classes.

        Returns
        -------
        dict : dict of best value and best Gini score.
        """
        class_values = list(np.unique(y))
        best_value, best_score, best_groups = 999, 999, None
        for value in X:
            # Get left and right group for value.
            groups = self.split_by_value_(value, X, y)
            # Get best Gini index.
            gini = self.get_gini_index_(groups, class_values)
            # Make sure gini core is in the right range.
            assert gini >= 0
            assert gini <= 0.5
            if gini < best_score:
                best_value, best_score, best_groups = value, gini, groups
        return {'value': best_value, 'gini': best_score}

    def fit(self, X, y):
        """
        Parameters
        ----------
        X : array-like or list, decision scores.

        y : array-like or list, list of classes.

        Returns
        -------
        self : instance of self object.
        """
        best = self.get_split_(X, y)
        self.threshold_ = best['value']
        self.best_gini_ = best['gini']
        return self

    def transform(self, X, y=None):
        """
        Method to assign class values to decision scores based on
        threshold calculated in the self.fit method.

        Parameters
        ----------
        X : array-like or list, decsion scores.

        Returns
        -------
        T : list of class values.
        """
        try:
            getattr(self, "threshold_")
        except AttributeError:
            raise RuntimeError("First fit, then transform!")

        return [x > self.threshold_ for x in X]


class custom_estimator(BaseEstimator, ClassifierMixin):
    """
    A custom estimator classbased on a logistic regression classifier
    for binary classification problems, on top of which a threshold binarizer
    could be used to find a Gini impurity optimized threshold for
    deciding between the final output classes.

    Parameters
    ----------

    Attributes
    ----------
    best_gini_ : float, best gini impurity index after running self.binarize.
    classes_ : array, shape (n_classes, )
        A list of class labels known to the classifier.
    coef_ : array, shape (1, n_features)
        Coefficient of the features in the decision function.
    lr_ : LogisticRegression instance.
    threshold_ : float, best threshold after running self.binarize.

    Examples
    --------
    >>> from sklearn.model_selection import train_test_split
    >>> X = np.array([[2.771244718, 1.784783929], [1.728571309, 1.169761413],
                      [3.678319846, 2.81281357], [3.961043357, 2.61995032],
                      [2.999208922, 2.209014212], [7.497545867, 3.162953546],
                      [9.00220326, 3.339047188], [7.444542326, 0.476683375],
                      [10.12493903, 3.234550982], [6.642287351, 3.319983761]])
    >>> y = np.array([0, 0, 0, 0, 0, 1, 1, 1, 1, 1])
    >>> X_train, X_test, y_train, y_test = train_test_split(X, y, stratify=y,
                                                            random_state=908)
    >>> from aliz_proba.threshold_binarizer import custom_estimator
    >>> CE = custom_estimator()
    >>> CE.fit(X_train, y_train)
    >>> y_pred = CE.binarize(X_train, X_test, y_train)
    >>> y_test
    [1, 0, 0]
    >>> y_pred
    [False, False, False]
    """
    def ___init___(self):
        pass

    def fit(self, X, y):
        """
        Fit linear model for binary classification. Uses fit method
        directly from the scikit-learn LinearRegression class.

        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples and
            n_features is the number of features.
        y : array-like, shape (n_samples,)
            Target vector relative to X

        Returns
        -------
        self : returns an instance of self.
        """
        lr = LogisticRegression()
        lr.fit(X, y)
        self.coef_ = lr.coef_
        self.classes_ = lr.classes_
        self.lr_ = lr
        return self

    def predict_proba(self, X):
        """
        Method to get probablity estimates.
        Results directly from the sckit-learn LogisticRegression class.

        Parameters
        ----------
        X : array-like, shape = [n_samples, n_features]
        Returns
        -------
        T : array-like, shape = [n_samples, n_classes]
            Returns the probability of the sample for each class in the model,
            where classes are ordered as they are in ``lr.classes_``.
        """
        return self.lr_.predict_proba(X)

    def predict(self, X):
        """
        Predict using the linear model self.lr_.

        Parameters
        ----------
        X : array_like or sparse matrix, shape (n_samples, n_features)
            Samples.

        Returns
        -------
        C : array, shape (n_samples,)
            Returns predicted values.
        """
        try:
            getattr(self, "coef_")
        except AttributeError:
            raise RuntimeError("First fit, then predict!")

        self.scores_ = self.lr_.decision_function(X)

        preds = self.lr_.predict(X)
        return preds

    def binarize(self, X_train, X_test, y_train):
        """
        Method to binarize/classify decision scores using the
        ThresholdBinarizer class to calculate the threshold.

        Parameters
        ----------
        X_train : array-like, shape (n_samples, n_features)
            Training vector, where n_samples is the number of samples and
            n_features is the number of features.
        X_test : array-like, shape (n_samples, n_features)
            Test vector, where n_samples is the number of samples and
            n_features is the number of features.
        y_train : array-like, shape (n_samples,)
            Target vector relative to X_train

        Returns
        -------
        y_pred : list of predicted class values for X_test.
        """
        try:
            getattr(self, "coef_")
        except AttributeError:
            raise RuntimeError("First fit, then binarize!")

        X_scores = self.lr_.decision_function(X_train)
        y_scores = self.lr_.decision_function(X_test)
        if isinstance(y_train, pd.core.series.Series):
            y_train = y_train.values

        TE = ThresholdBinarizer()
        TE.fit(X_scores, y_train)
        y_pred = TE.transform(y_scores)

        self.best_gini_ = TE.best_gini_
        self.threshold_ = TE.threshold_
        return y_pred
