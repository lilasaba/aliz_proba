#!/bin/bash

ENV=aliz

source deactivate
conda env remove --name $ENV --yes
conda create --name $ENV python=3.6 --yes
source activate $ENV
conda info -e
python setup.py install
