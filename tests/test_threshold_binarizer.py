"""Tests for `aliz_proba` package."""

import numpy as np
import pandas as pd
import sys
import unittest

from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split

sys.path.insert(0, 'tests')
from aliz_proba import threshold_binarizer
from tests.expected_data import expected

try:
    from local_config import local_config
except ModuleNotFoundError:
    local_config = False

# Read in data csv to pandas df.
df = pd.read_csv('data/breast_cancer.csv')
# Binarize class labels; by default majority class (benign)
# will be negative.
lb = LabelBinarizer()
# Create targets (y) df.
df['diagnosis'] = lb.fit_transform(df['diagnosis'].values)
targets = df['diagnosis']
# Drop id and atrget columns from df.
df.drop(['id', 'diagnosis'], axis=1, inplace=True)
#print(df.head)
# Train-test split with equal distribution of the classes.
X_train, X_test, y_train, y_test = train_test_split(df, targets,
                                                    stratify=targets,
                                                    random_state=908)


class TestThreshold(unittest.TestCase):
    """Tests for `aliz_proba` package."""

    def setUp(self):
        """Set up test fixtures, if any."""
        CE = threshold_binarizer.custom_estimator()
        CE.fit(X_train, y_train)
        self.CE_ = CE

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_before_binarizing(self):
        """
        """
        y_pred_expected = expected[:]
        y_pred = list(self.CE_.predict(X_test))
        self.assertEqual(y_pred_expected, y_pred)

    def test_after_binarizing(self):
        """
        """
        y_pred = self.CE_.binarize(X_train, X_test, y_train)
        expected_gini = 0.0622
        if local_config:
            expected_threshold = -0.7243
        else:
            expected_threshold = -0.7362
        self.assertEqual(expected_gini, float('%.4f' % self.CE_.best_gini_))
        self.assertAlmostEqual(expected_threshold, float('%.4f' % self.CE_.threshold_))

if __name__ == '__main__':
    unittest.main()
