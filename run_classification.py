# vim: tabstop=8 expandtab shiftwidth softtabstop=4

import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.metrics import (accuracy_score, confusion_matrix,
                             precision_score, recall_score)
from sklearn.preprocessing import LabelBinarizer

from aliz_proba import threshold_binarizer

# Read in data csv to pandas df.
df = pd.read_csv('data/breast_cancer.csv')
# Binarize class labels; by default majority class (benign)
# will be negative.
lb = LabelBinarizer()
# Create targets (y) df.
df['diagnosis'] = lb.fit_transform(df['diagnosis'].values)
targets = df['diagnosis']
# Drop id and atrget columns from df.
df.drop(['id', 'diagnosis'], axis=1, inplace=True)
# Train-test split with equal distribution of the classes.
X_train, X_test, y_train, y_test = train_test_split(df, targets,
                                                    stratify=targets,
                                                    random_state=908)


def print_results(y_true, y_pred):
    print(pd.DataFrame(confusion_matrix(y_true, y_pred),
                       columns=['pred_neg', 'pred_pos'], index=['neg', 'pos']))
    print('Recall: %.4f' % recall_score(y_true, y_pred))
    print('Precision: %.4f' % precision_score(y_true, y_pred))
    print('Accuracy: %.4f' % accuracy_score(y_true, y_pred))


if __name__ == '__main__':
    # Create classifier instance.
    CE = threshold_binarizer.custom_estimator()
    CE.fit(X_train, y_train)
    y_pred = CE.predict(X_test)
    print('*'*50)
    print('*'*50)
    print('Result before threshold binarizing:')
    print_results(y_test, y_pred)
    print('*'*50)
    print('*'*50)
    # Get Gini impurity optimized threshold, and refit class predictions.
    y_pred = CE.binarize(X_train, X_test, y_train)
    print('Best split at %.4f.' % CE.threshold_)
    print('Gini: %.4f.' % CE.best_gini_)
    print('*'*50)
    print('Result after threshold binarizing:')
    print_results(y_test, y_pred)
    print('*'*50)
    print('*'*50)
