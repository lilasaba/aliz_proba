# Aliz assignement

Python package of a linear regression based classifier with
threshold binarizer optimized for Gini impurity.

## Python version

`3.6`

## Install

```
python setup.py install
```

## Import

```
from aliz_proba.threshold_binarizer import custom_estimator
from aliz_proba.threshold_binarizer import ThresholdBinarizer
```

## Test

```
python setup.py test
```

## Usage

On toy dataset in `data/breast_cancer.csv`:

```
pip install -r requirements.txt
python run_classification.py
```

## Notes

The results on the toy dataset show that the Gini impurity driven threshold
optimization helps with the recall but not with the precision.

## Credits

This package was created with Cookiecutte and the `audreyr/cookiecutter-pypackage` project template.
